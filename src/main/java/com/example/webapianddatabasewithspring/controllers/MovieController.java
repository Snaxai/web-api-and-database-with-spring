package com.example.webapianddatabasewithspring.controllers;

import com.example.webapianddatabasewithspring.models.Character;
import com.example.webapianddatabasewithspring.models.Movie;
import com.example.webapianddatabasewithspring.repositories.CharacterRepository;
import com.example.webapianddatabasewithspring.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/movie")
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CharacterRepository characterRepository;

    /**
     * Get all movies
     *
     * @return list of all movies
     */
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies, status);
    }

    /**
     * Get movie by id
     *
     * @param id id of movie
     * @return the movie
     */
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable int id) {
        Movie returnMovie = new Movie();
        HttpStatus status;
        if (movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnMovie = movieRepository.findById(id).get();
        } else status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Add a movie
     *
     * @param movie movie data
     * @return the movie
     */
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        HttpStatus status;
        movie = movieRepository.save(movie);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(movie, status);
    }

    /**
     * Update a movie
     *
     * @param id    id of the movie
     * @param movie the updated data of the movie
     * @return the updated movie
     */
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable int id, @RequestBody Movie movie) {
        Movie returnMovie = new Movie();
        HttpStatus status;
        if (!(id == (movie.getId()))) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie, status);
        }
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;

        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Delete a movie
     *
     * @param id id of the movie
     * @return a response message
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMovie(@PathVariable int id) {
        String responseMessage = "";
        HttpStatus status;
        if (movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            movieRepository.deleteById(id);
            responseMessage = "Deleted movie with id: " + id;
        } else {
            status = HttpStatus.NOT_FOUND;
            responseMessage = "Could not find movie with id: " + id;
        }

        return new ResponseEntity<>(responseMessage, status);
    }

    /**
     * Get all characters in a movie
     *
     * @param id id of the movie
     * @return a set of characters
     */
    @GetMapping("/{id}/all/characters")
    public ResponseEntity<Set<Character>> getAllCharactersInMovie(@PathVariable int id) {
        Set<Character> characters = movieRepository.getById(id).getCharacters();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters, status);
    }

    /**
     * Update or add characters in a movie
     *
     * @param id           id of the movie
     * @param characterIds ids of characters
     * @return The characters being added
     */
    @PutMapping("/{id}/characters")
    public ResponseEntity<Set<Character>> updateCharactersInMovie(@PathVariable int id, @RequestBody int[] characterIds) {
        Movie movie = movieRepository.findById(id).get();
        HttpStatus status;
        HashSet<Character> characters = new HashSet<>();
        for (int characterId : characterIds) {
            Character character = characterRepository.findById(characterId).get();
            characters.add(character);
        }
        movie.setCharacters(characters);
        movieRepository.save(movie);
        status = HttpStatus.OK;
        return new ResponseEntity<>(characters, status);
    }
}
