package com.example.webapianddatabasewithspring.controllers;

import com.example.webapianddatabasewithspring.models.Character;
import com.example.webapianddatabasewithspring.models.Franchise;
import com.example.webapianddatabasewithspring.models.Movie;
import com.example.webapianddatabasewithspring.repositories.FranchiseRepository;
import com.example.webapianddatabasewithspring.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/franchise")
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieRepository movieRepository;

    /**
     * Get all franchises
     * @return List of franchises
     */
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    /**
     * Get franchise by id
     * @param id the id of the franchise
     * @return the added franchise
     */
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchiseById(@PathVariable int id) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
        } else status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * Add a new franchise
     * @param franchise the data of the franchise
     * @return the franchise
     */
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        HttpStatus status;
        franchise = franchiseRepository.save(franchise);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(franchise, status);
    }

    /**
     * Update a franchise
     * @param id id of the franchise
     * @param franchise the updated data of the franchise
     * @return the updated franchise
     */
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable int id, @RequestBody Franchise franchise) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        if (!(id == (franchise.getId()))) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise, status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * Delete a franchise
     * @param id id of the franchise
     * @return a response message
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFranchise(@PathVariable int id) {
        String responseMessage = "";
        HttpStatus status;
        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            franchiseRepository.deleteById(id);
            responseMessage = "Deleted franchise with id: " + id;
        } else {
            status = HttpStatus.NOT_FOUND;
            responseMessage = "Could not find franchise with id: " + id;
        }
        return new ResponseEntity<>(responseMessage, status);
    }

    /**
     * Get all characters in a franchise
     * @param id id of the franchise
     * @return list of characters
     */
    @GetMapping("/{id}/all/characters")
    public ResponseEntity<Set<Character>> getAllCharactersInFranchise(@PathVariable int id) {
        List<Movie> movies = franchiseRepository.findById(id).get().getMovies();
        Set<Character> characters = new HashSet<>();
        for (Movie movie : movies) {
            characters.addAll(movie.characters);
        }
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters, status);
    }

    /**
     * Get all movies in a franchise
     * @param id id if the franchise
     * @return list of movies
     */
    @GetMapping("/{id}/all/movies")
    public ResponseEntity<List<Movie>> getAllMoviesInFranchise(@PathVariable int id) {
        List<Movie> movies = franchiseRepository.getById(id).getMovies();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies, status);
    }

    /**
     * Update or add movies to a franchise
     * @param id id of the franchise
     * @param movieIds movie ids being added to the franchise
     * @return the added movies
     */
    @PutMapping("/{id}/movies")
    public ResponseEntity<List<Movie>> updateMoviesInFranchise(@PathVariable int id, @RequestBody int[] movieIds) {
        Franchise franchise = franchiseRepository.findById(id).get();
        HttpStatus status;
        List<Movie> movies = new ArrayList<>();
        for (int movieId : movieIds) {
            Movie movie = movieRepository.findById(movieId).get();
            movies.add(movie);
        }
        franchise.setMovies(movies);
        franchiseRepository.save(franchise);
        status = HttpStatus.OK;
        return new ResponseEntity<>(movies, status);
    }
}
