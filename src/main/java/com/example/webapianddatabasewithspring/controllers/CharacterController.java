package com.example.webapianddatabasewithspring.controllers;

import com.example.webapianddatabasewithspring.models.Character;
import com.example.webapianddatabasewithspring.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/character")
public class CharacterController {
    @Autowired
    private CharacterRepository characterRepository;

    /**
     * Get all characters
     * @return all characters
     */
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters() {
        List<Character> characters = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters, status);
    }

    /**
     * Get a character by id
     * @param id id if the character
     * @return the character
     */
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacterById(@PathVariable int id) {
        Character returnCharacter = new Character();
        HttpStatus status;
        if (characterRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnCharacter = characterRepository.findById(id).get();
        } else status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(returnCharacter, status);
    }

    /**
     * Add a new character
     * @param character the character data
     * @return The added character and status code
     */
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character) {
        HttpStatus status;
        character = characterRepository.save(character);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(character, status);
    }

    /**
     * Update a character
     * @param id id of the character
     * @param character the updated data of the character
     * @return BAD_REQUEST or NO_CONTENT
     */
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable int id, @RequestBody Character character) {
        Character returnCharacter = new Character();
        HttpStatus status;
        if (!(id == (character.getId()))) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter, status);
        }
        returnCharacter = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;

        return new ResponseEntity<>(returnCharacter, status);
    }

    /**
     * Delete a character
     * @param id id if the character to delete
     * @return response message
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCharacter(@PathVariable int id) {
        String responseMessage = "";
        HttpStatus status;
        if (characterRepository.existsById(id)) {
            status = HttpStatus.OK;
            characterRepository.deleteById(id);
            responseMessage = "Deleted character with id: " + id;
        } else {
            status = HttpStatus.NOT_FOUND;
            responseMessage = "Could not find character with id: " + id;
        }
        return new ResponseEntity<>(responseMessage, status);
    }
}
