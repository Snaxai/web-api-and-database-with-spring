package com.example.webapianddatabasewithspring.repositories;

import com.example.webapianddatabasewithspring.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
