package com.example.webapianddatabasewithspring.repositories;

import com.example.webapianddatabasewithspring.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterRepository extends JpaRepository<Character, Integer> {
}
