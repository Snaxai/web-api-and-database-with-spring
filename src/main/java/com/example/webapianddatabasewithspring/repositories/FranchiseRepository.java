package com.example.webapianddatabasewithspring.repositories;

import com.example.webapianddatabasewithspring.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
}
