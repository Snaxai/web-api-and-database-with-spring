package com.example.webapianddatabasewithspring.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String alias;
    private String gender;
    private URL picture;

    @ManyToMany(mappedBy = "characters")
    public Set<Movie> movies;

    @JsonGetter("movies")
    public List<String> moviesGetter() {
        if (movies != null) {
            return movies.stream().map(movie -> "/api/v1/movie/"
                            + movie.getId())
                    .collect(Collectors.toList());
        }
        return null;
    }

    public Character() {
    }

    public Character(String name, String alias, String gender, URL picture) {
        this.name = name;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
    }

    public Character(int id, String name, String alias, String gender, URL picture, Set<Movie> movies) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
        this.movies = movies;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public URL getPicture() {
        return picture;
    }

    public void setPicture(URL picture) {
        this.picture = picture;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
