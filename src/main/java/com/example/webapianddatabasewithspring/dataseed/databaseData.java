package com.example.webapianddatabasewithspring.dataseed;

import com.example.webapianddatabasewithspring.models.Character;
import com.example.webapianddatabasewithspring.models.Franchise;
import com.example.webapianddatabasewithspring.models.Movie;
import com.example.webapianddatabasewithspring.repositories.CharacterRepository;
import com.example.webapianddatabasewithspring.repositories.FranchiseRepository;
import com.example.webapianddatabasewithspring.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class databaseData implements CommandLineRunner {

    @Autowired
    CharacterRepository characterRepository;

    @Autowired
    FranchiseRepository franchiseRepository;

    @Autowired
    MovieRepository movieRepository;

    @Override
    public void run(String... args) throws Exception {
        loadCharacterData();
        loadMoviesData();
        loadFranchiseData();
    }

    /**
     * load some character data to the database
     */
    private void loadCharacterData() {
        if (characterRepository.count() == 0) {
            Character character1 = new Character("Daniel", "Dan", "Male", null);
            characterRepository.save(character1);
            Character character2 = new Character("Ola", "Ola", "Male", null);
            characterRepository.save(character2);
            Character character3 = new Character("Silje", "The girl", "Female", null);
            characterRepository.save(character3);
        }
    }

    /**
     * load some movies data to the database
     */
    private void loadMoviesData() {
        if (movieRepository.count() == 0) {
            Movie movie1 = new Movie("Movie1", "Drama", 1999, "man", null, null);
            movieRepository.save(movie1);
            Movie movie2 = new Movie("Movie2", "Action", 2005, "man", null, null);
            movieRepository.save(movie2);
            Movie movie3 = new Movie("Movie3", "Drama", 2003, "man", null, null);
            movieRepository.save(movie3);
        }
    }

    /**
     * load some franchise data to the database
     */
    private void loadFranchiseData() {
        if (franchiseRepository.count() == 0) {
            Franchise franchise1 = new Franchise("asd", "some desc");
            franchiseRepository.save(franchise1);
            Franchise franchise2 = new Franchise("das", "some desc");
            franchiseRepository.save(franchise2);
        }
    }
}
