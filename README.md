### Assignment 3 Java - Create Web API and database with Spring
In this assignment i will create a web API with Spring that uses a postgresql database

## Documentation with Swagger
This project is documented with the use of Open API v3 Swagger

```https://web-api-with-spring-and-postgr.herokuapp.com/swagger-ui/index.html```

## Heroku
The project is hosted on Heroku

```https://web-api-with-spring-and-postgr.herokuapp.com/```

## Database
The database is hosted on Heroku

```https://data.heroku.com/datastores/60c1c101-473a-4ec5-bb8e-649624af4f7d#```

## Postman
A collection of test requests made in postman is available

```https://gitlab.com/Snaxai/web-api-and-database-with-spring/-/blob/main/postman%20collection/Assignment%203%20Java%20Web%20API%20with%20Spring%20and%20postgresql.postman_collection.json```

## Author
Daniel Mossestad
